from turtle import width
import requests
import json
import typer
import shippo


app = typer.Typer()

@app.command()
def makeparcel(): 
    shippo.config.api_key = input("Enter your api key here ")
    length = input("Enter the parcel length(number only) ")
    width = input("Enter the parcel width(number only) ")
    height = input("Enter the parcel height(number only) ")
    distance_unit = input("Enter the distance unit(ex. cm or in ")
    weight = input("Enter the parcel weight(number only) ")
    mass_unit = input("Enter the mass unit here(ex. kg or lb ")
    parcel = shippo.Parcel.create(
    length=length,
    width=width,
    height=height,
    distance_unit=distance_unit,
    weight=weight,
    mass_unit=mass_unit
    )
    print('Success with parcel : %r' % (parcel, ))

@app.command()
def makeaddress(): 
    shippo.config.api_key = input("Enter your api key here ")
    name = input("Enter the name associated with the address ")
    street1 = input("Enter the street address")
    street2 = input("Enter additional street info ex.appt number(optional) ")
    city = input("Enter the city ")
    state = input("Enter the state ")
    zip = input("Enter the zip ")
    country = input("Enter the country ")
    phone = input("Enter the phone number ")
    company = input("Enter the company associated with the address(optional) ")
    metadata = input("Enter any extra information you would like attached to the address ex.customer id(optional) ")
    
    address1 = shippo.Address.create(
    name=name,
    street1=street1,
    street2=street2,
    company=company,
    phone=phone,
    city=city,
    state=state,
    zip=zip,
    country=country,
    metadata=metadata
)
    print('Success with Address 1 : %r' % (address1, ))


@app.command()
def makeshipment(): 
    shippo.config.api_key = input("Enter your api key here ")
    address_to = input("Enter the address id for the address your package is going to ")
    address_from = input("Enter the address id for the address your package is coming from ")
    parcel = input("Enter the parcel object id(s) for your parcel(s). If multiple, make sure to add a single space between id's ")
    parcels = parcel.split()
    shipment = shippo.Shipment.create(address_from=address_from, address_to=address_to, parcels=parcels)
    print('Success with shipment : %r' % (shipment, ))

@app.command()
def retrieveshipment(): 
    shippo.config.api_key = input("Enter your api key here ")
    object_id = input("Enter your shipment object id here ")
    shipment = shippo.Shipment.retrieve(object_id)
    print('Success with shipment : %r' % (shipment, ))

@app.command()
def retrieveparcel(): 
    shippo.config.api_key = input("Enter your api key here ")
    object_id = input("Enter your parcel object id here ")
    parcel = shippo.Parcel.retrieve(object_id)
    print('Success with parcel : %r' % (parcel, ))

@app.command()
def retrieverates(): 
    api_key = input("Enter your api key here ")
    object_id = input("Enter your shipment object id here ")
    currency_code = input("Enter the currency code here(ex. USD) ")
    headers = {"Authorization": f"ShippoToken {api_key}", "Content-Type": "application/json"}
    r = requests.get(f"https://api.goshippo.com/shipments/{object_id}/rates/{currency_code}", headers=headers)
    if r.ok is False:
        print("There is a problem with one of your inputs. Please reconfirm input values and try again.")
    else:
        print(r.text)

@app.command()
def retrieveshipmentaddresses(): 
    api_key = input("Enter your api key here ")
    object_id = input("Enter your shipment object id here ")
    headers = {"Authorization": f"ShippoToken {api_key}", "Content-Type": "application/json"}
    r = requests.get(f"https://api.goshippo.com/shipments/{object_id}", headers=headers)
    if r.ok is False:
        print("There is a problem with one of your inputs. Please reconfirm input values and try again.")
    else:
        response = json.loads(r.text)
        print({"address_from": response["address_from"], "address_to": response["address_to"]})


@app.command()
def retrievesenderorreceiver(): 
    shippo.config.api_key = input("Enter your api key here ")
    object_id = input("Enter the address object id here ")
    address = shippo.Address.retrieve(object_id)
    print('Success with Address : %r' % (address, ))


if __name__ == "__main__":
    app()