# Shippo api tool

For this technical take home assignment I created a cli tool that completes the following tasks listed below:
1. Create a shipment
2. Retrieve a shipment
3. Retrieve rates for a shipment
4. Retrieve either the sender or recipient address by specifying their object ids or retrieve
both simultaneously by passing a shipment’s object id
5. Retrieve a parcel by specifying its object id or by passing a shipment’s object id

In order to acheive this I used typer in order to create commands that execute functions that perform the above tasks. The command names are:
1. makeshipment - used to create and display shipment details in CLI. Use makeaddress and makeparcel commands to create addresses and parcels for makeshipment. The object id's created in makeaddress and makeparcel will be used in makeshipment
2. makeaddress - generates address object
3. makeparcel - generates parcel object
4. retrieveshipment - retreives shipment details (requires shipment object id)
5. retrieveparcel - retreives parcel details (requires parcel object id)
6. retrieverates - retreives rates details (requires shipment object id)
7. retrieveshipmentaddresses - generates sender and receiver address details (requires shipment object id)
8. retrievesenderorreceiver - retreive either sender or receiver address details (requires address object id)


## Usage

 - unzip the folder and add to your local directory
 - using your terminal, cd into the folder
 - using mac activate the virtual environment with this script: source ./.venv/bin/activate
 - upgrade pip and install requirements with these two scripts on after the other: python -m pip install --upgrade pip  then pip install -r requirements.txt
 - from the the terminal in order to run the cli tool commands type python main.py {command}. ex. python main.py makeaddress
 - answer input questions when prompted, do not add extra space in response.
 - enjoy!

 ## SDK Usage and review
 - For this cli tool I installed and used shippo's python client library. Overall I liked using the shippo library, the only issue was I wasn't able to use it with pypi's latest version of requests and had to downgrade the requests version in order to use the shippo sdk. 




